<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\image\grabber\tests;
use alexs\image\grabber\Grabber;
use PHPUnit\Framework\TestCase;

class GrabberTest extends TestCase
{
    /**
     * @expectedException \RuntimeException
     */
    public function testGrabInvalidUrl() {
        $url = 'invalid_url';
        Grabber::grab($url);
    }

    public function testGrab() {
        $url = 'https://lh4.googleusercontent.com/-v0soe-ievYE/AAAAAAAAAAI/AAAAAAADwkE/KyrKDjjeV1o/photo.jpg';
        $raw = Grabber::grab($url);
        $this->assertInternalType('string', $raw);
    }

    public function testGrabUpload() {
        $url = 'https://lh4.googleusercontent.com/-v0soe-ievYE/AAAAAAAAAAI/AAAAAAADwkE/KyrKDjjeV1o/photo.jpg';
        $filename = basename($url);
        Grabber::grabUpload($url, basename($url));
        $this->assertFileExists($filename);
        @unlink($filename);
    }
}