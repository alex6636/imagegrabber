# README #

Image Grabber

### Example ###

```php
<?php
use alexs\image\grabber\Grabber;

$url = 'https://lh4.googleusercontent.com/-v0soe-ievYE/AAAAAAAAAAI/AAAAAAADwkE/KyrKDjjeV1o/photo.jpg';
var_dump(Grabber::grab($url));
var_dump(Grabber::grabUpload($url, basename($url)));
```