<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   31-Aug-17
 *
 * Image Grabber
 */

namespace alexs\image\grabber;
use RuntimeException;

class Grabber
{
    public static function grab($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $raw = curl_exec($ch);
        if ($raw === false) {
            throw new RuntimeException(curl_error($ch));
        }
        curl_close($ch);
        return $raw;
    }

    public static function grabUpload($url, $upload_path) {
        $raw = static::grab($url);
        $fp = fopen($upload_path, 'x');
        $result = fwrite($fp, $raw);
        fclose($fp);
        return $result;
    }
}